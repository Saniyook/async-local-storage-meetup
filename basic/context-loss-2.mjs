// https://github.com/nodejs/node/issues/42237
import { AsyncLocalStorage } from "async_hooks";

const store = new AsyncLocalStorage();

async function normal() {
  console.log("async fn", store.getStore());
}

async function* gen() {
  console.log("async gen", store.getStore());
}

async function* gen2() {
  console.log("async gen2", store.getStore());
}

store.run("hello", normal);

store.run("hello", gen).next();

store.run("hello2", async () => {
  await gen2().next();
});
