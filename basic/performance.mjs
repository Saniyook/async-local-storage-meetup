// https://github.com/nodejs/node/issues/34493
import { AsyncLocalStorage } from "async_hooks";
const asyncLocalStorage = new AsyncLocalStorage();

// let fn = () => /test/.test("test");
// Performed 353260 iterations to warmup
// Performed 2407768 iterations (with ALS enabled)
// Performed 4989094 iterations (with ALS disabled)
// ALS penalty: 51.74%

// let fn = () => Promise.resolve("test");
// Performed 574543 iterations to warmup
// Performed 2659048 iterations (with ALS enabled)
// Performed 6009847 iterations (with ALS disabled)
// ALS penalty: 55.76%

// let fn = () => new Promise(setImmediate);
// Performed 41285 iterations to warmup
// Performed 472644 iterations (with ALS enabled)
// Performed 498903 iterations (with ALS disabled)
// ALS penalty: 5.26%

let fn = () => new Promise(process.nextTick);
// Performed 134967 iterations to warmup
// Performed 1119798 iterations (with ALS enabled)
// Performed 1734778 iterations (with ALS disabled)
// ALS penalty: 35.45%

let runWithExpiry = async (expiry, fn) => {
  let iterations = 0;
  while (Date.now() < expiry) {
    await fn();
    iterations++;
  }
  return iterations;
};

(async () => {
  console.log(
    `Performed ${await runWithExpiry(
      Date.now() + 100,
      fn
    )} iterations to warmup`
  );

  asyncLocalStorage.run({}, () => {});
  let withAls = await runWithExpiry(Date.now() + 1000, fn);
  console.log(`Performed ${withAls} iterations (with ALS enabled)`);

  asyncLocalStorage.disable();
  let withoutAls = await runWithExpiry(Date.now() + 1000, fn);
  console.log(`Performed ${withoutAls} iterations (with ALS disabled)`);

  console.log(
    "ALS penalty: " + Math.round((1 - withAls / withoutAls) * 10000) / 100 + "%"
  );
})();
