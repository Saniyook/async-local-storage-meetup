// https://github.com/nodejs/node/issues/45848
import { AsyncLocalStorage } from "node:async_hooks";
import { equal } from "assert";

const store = new AsyncLocalStorage();

const unrelatedStore = new AsyncLocalStorage();
// the test passes if you comment this out
unrelatedStore.enterWith("unrelated");

// oversimplification of jest internals 😅
async function testHarness() {
  // the test also passes if you enter synchronously, e.g. change to
  // `store.enterWith("value")` or `await new Promise((resolve) => resolve(store.enterWith("value")));`
  await Promise.resolve().then(() => store.enterWith("value"));
  equal(unrelatedStore.getStore(), "unrelated");
  unrelatedStore.exit(() => {
    unrelatedStore.enterWith("unrelated");
    equal(unrelatedStore.getStore(), "unrelated");
  });
  equal(store.getStore(), "value");
}
testHarness();
