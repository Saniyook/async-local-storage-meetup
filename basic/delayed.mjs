import http from "node:http";
import { AsyncLocalStorage } from "node:async_hooks";

const asyncLocalStorage = new AsyncLocalStorage();

function logWithId(msg) {
  const { traceId } = asyncLocalStorage.getStore();
  console.log(`${traceId !== undefined ? traceId : "-"}:`, msg);
}

const hostname = "127.0.0.1";
const port = 3000;

let requestId = 0;
const server = http.createServer((req, res) => {
  asyncLocalStorage.run({ traceId: requestId++ }, () => {
    logWithId("start");

    // Imagine any chain of async operations here
    setImmediate(() => {
      logWithId("finish");
      res.end();
    });

    setTimeout(() => {
      logWithId("delayed-1");
    }, 2000);

    process.nextTick(() => {
      logWithId("nextTick");
    });
  });
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});

http.get(`http://${hostname}:${port}`);
http.get(`http://${hostname}:${port}`);
// Prints:
// {0}: start
// {0}: nextTick
// {1}: start
// {1}: nextTick
// {0}: finish
// {1}: finish
// {0}: delayed
// {1}: delayed
