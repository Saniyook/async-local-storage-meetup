export class UserDto {
  id: string;

  constructor(data: UserDto) {
    Object.assign(this, data);
  }
}
