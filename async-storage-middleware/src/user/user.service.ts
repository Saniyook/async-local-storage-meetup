import {
  Logger,
  Injectable,
  InternalServerErrorException,
} from '@nestjs/common';

import { UserDto } from './dto/user.dto';
import { AsyncStorageService } from '../async-storage/async-storage.service';

@Injectable()
export class UserService {
  private readonly logger = new Logger(UserService.name);

  constructor(private readonly asyncStorageService: AsyncStorageService) {}

  async getMe(): Promise<UserDto> {
    const userId = this.asyncStorageService.get('userId');

    if (!userId) {
      throw new InternalServerErrorException('ERROR_CODE');
    }

    const traceId = this.asyncStorageService.get('traceId') as string;

    this.logger.log(`Get me called userId: ${userId}, traceId: ${traceId}`);

    return new UserDto({ id: userId });
  }
}
