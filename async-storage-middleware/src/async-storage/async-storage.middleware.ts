import { randomUUID } from 'node:crypto';
import { Injectable, NestMiddleware } from '@nestjs/common';

import { AsyncStorageService } from './async-storage.service';

@Injectable()
export class AsyncStorageMiddleware implements NestMiddleware {
  constructor(private readonly asyncStorageService: AsyncStorageService) {}

  use(req: any, res: any, next: (error?: any) => void) {
    this.asyncStorageService.run({ traceId: randomUUID() }, () => next());
  }
}
