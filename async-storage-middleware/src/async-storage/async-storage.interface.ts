export interface AsyncStore {
  traceId: string;
  userId?: string;
}
