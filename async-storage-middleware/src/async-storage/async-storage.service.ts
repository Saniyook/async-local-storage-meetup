import { Injectable, Scope } from '@nestjs/common';
import { AsyncLocalStorage } from 'async_hooks';

import { AsyncStore } from './async-storage.interface';

@Injectable({ scope: Scope.DEFAULT })
export class AsyncStorageService {
  private readonly store = new AsyncLocalStorage<AsyncStore>();

  run(store: AsyncStore, cb: (...args: any) => any) {
    this.store.run(store, cb);
  }

  set<Key extends keyof AsyncStore>(key: Key, value: AsyncStore[Key]) {
    const store = this.store.getStore();

    if (store) {
      store[key] = value;

      return true;
    }

    return false;
  }

  get<Key extends keyof AsyncStore>(key: Key): AsyncStore[Key] | undefined {
    const store = this.store.getStore();

    if (store) {
      return store[key];
    }
  }
}
