import {
  Logger,
  Injectable,
  CanActivate,
  ExecutionContext,
} from '@nestjs/common';
import { Request } from 'express';
import { AsyncStorageService } from '../async-storage/async-storage.service';

@Injectable()
export class AuthGuard implements CanActivate {
  private readonly logger = new Logger(AuthGuard.name);

  constructor(private readonly asyncStorageService: AsyncStorageService) {}

  async canActivate(context: ExecutionContext) {
    const type = context.getType();

    if (type !== 'http') {
      return false;
    }

    const request = context.switchToHttp().getRequest<Request>();

    const userId = this.getTokenFromRequest(request);

    if (!userId || typeof userId !== 'string') {
      return false;
    }

    const traceId = this.asyncStorageService.get('traceId') as string;

    this.logger.log(`Success login userId: ${userId}, traceId: ${traceId}`);

    this.asyncStorageService.set('userId', userId);

    return true;
  }

  private getTokenFromRequest(request: Request) {
    return request.headers['x-user-id'];
  }
}
