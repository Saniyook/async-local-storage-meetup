import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';

import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';
import { AsyncStorageModule } from './async-storage/async-storage.module';
import { AsyncStorageMiddleware } from './async-storage/async-storage.middleware';

@Module({
  imports: [AsyncStorageModule, UserModule, AuthModule],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(AsyncStorageMiddleware).forRoutes('*');
  }
}
