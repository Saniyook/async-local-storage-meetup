import { randomUUID } from 'crypto';
import * as request from 'supertest';
import { INestApplication, Logger } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';

import { AppModule } from './../src/app.module';
import { UserDto } from '../src/user/dto/user.dto';
import { UserController } from '../src/user/user.controller';

describe(`${UserController.name} (e2e)`, () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
    app.useLogger(new Logger());
  });

  it('/user/me (GET)', (done) => {
    const userId = randomUUID();

    const userDto = new UserDto({ id: userId });

    request(app.getHttpServer())
      .get('/user/me')
      .set({ ['x-user-id']: userId })
      .expect(200)
      .end((err, res) => {
        expect(res.body).toEqual(userDto);
        done();
      });
  });
});
