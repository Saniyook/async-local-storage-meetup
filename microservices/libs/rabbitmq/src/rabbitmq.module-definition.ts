import { ConfigurableModuleBuilder } from '@nestjs/common';
import { RabbitMQModuleOptions } from './rabbitmq.interfaces';

export const {
  ConfigurableModuleClass: Configurable,
  MODULE_OPTIONS_TOKEN: RABBITMQ_MODULE_OPTIONS,
  ASYNC_OPTIONS_TYPE: RabbitMQModuleAsyncOptions,
} = new ConfigurableModuleBuilder<RabbitMQModuleOptions>()
  .setClassMethodName('forRoot')
  .build();
