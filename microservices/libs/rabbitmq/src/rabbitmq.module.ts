import { DynamicModule, Global, Module, Provider, Type } from '@nestjs/common';
import { RabbitMQModule as GoLevelUpRabbitMQModule } from '@golevelup/nestjs-rabbitmq';

import {
  Configurable,
  RABBITMQ_MODULE_OPTIONS,
} from './rabbitmq.module-definition';
import { UserEventsRabbitMQService } from './user';
import { OrderEventsRabbitMQService } from './order';
import { RabbitMqPublisher } from './rabbitmq.publisher';
import {
  RabbitMQModuleAsyncOptions,
  RabbitMQModuleOptions,
  RabbitMQModuleOptionsFactory,
} from './rabbitmq.interfaces';
import { Exchanges, ExchangesTypesMap } from './const/exchanges.enum';
import { AuthEventsRabbitMQService } from './auth';

const services = [
  UserEventsRabbitMQService,
  OrderEventsRabbitMQService,
  AuthEventsRabbitMQService,
];

@Global()
@Module({})
export class RabbitMQModule {
  static forRoot(options: RabbitMQModuleOptions): DynamicModule {
    return {
      module: RabbitMQModule,
      imports: [
        GoLevelUpRabbitMQModule.forRoot(GoLevelUpRabbitMQModule, {
          uri: options.uri,
          exchanges: Object.values(Exchanges).map((exchange) => ({
            name: exchange,
            type: ExchangesTypesMap[exchange],
            options: {
              durable: false,
            },
          })),
        }),
      ],
      providers: [
        ...services,
        RabbitMqPublisher,
        { provide: RABBITMQ_MODULE_OPTIONS, useValue: options },
      ],
      exports: [GoLevelUpRabbitMQModule, ...services],
      global: true,
    };
  }

  static forRootAsync(options: RabbitMQModuleAsyncOptions): DynamicModule {
    const asyncProviders = this.createAsyncProviders(options);

    return {
      module: RabbitMQModule,
      imports: [
        GoLevelUpRabbitMQModule.forRootAsync(GoLevelUpRabbitMQModule, {
          inject: [RABBITMQ_MODULE_OPTIONS],
          useFactory: (moduleOptions: RabbitMQModuleOptions) => ({
            uri: moduleOptions.uri,
            exchanges: Object.values(Exchanges).map((exchange) => ({
              name: exchange,
              type: ExchangesTypesMap[exchange],
              options: {
                durable: false,
              },
            })),
          }),
        }),
        ...(options.imports || []),
      ],
      providers: [...asyncProviders, ...services, RabbitMqPublisher],
      exports: [GoLevelUpRabbitMQModule, ...asyncProviders, ...services],
      global: true,
    };
  }

  private static createAsyncProviders(
    options: RabbitMQModuleAsyncOptions
  ): Provider[] {
    if (options.useExisting || options.useFactory) {
      return [this.createAsyncOptionsProvider(options)];
    }
    const useClass = options.useClass as Type<RabbitMQModuleOptionsFactory>;
    return [
      this.createAsyncOptionsProvider(options),
      {
        provide: useClass,
        useClass,
      },
    ];
  }

  private static createAsyncOptionsProvider(
    options: RabbitMQModuleAsyncOptions
  ): Provider {
    if (options.useFactory) {
      return {
        provide: RABBITMQ_MODULE_OPTIONS,
        // eslint-disable-next-line prefer-spread
        useFactory: (...args) => (options.useFactory as any).apply(null, args),
        inject: options.inject || [],
      };
    }
    const inject = [
      (options.useClass ||
        options.useExisting) as Type<RabbitMQModuleOptionsFactory>,
    ];
    return {
      provide: RABBITMQ_MODULE_OPTIONS,
      useFactory: (optionsFactory: RabbitMQModuleOptionsFactory) =>
        optionsFactory.createRabbitMQModuleOptions(),
      inject,
    };
  }
}
