export enum Exchanges {
  USER_EVENTS = 'user_events',
  ORDER_EVENTS = 'order_events',
  AUTH_EVENTS = 'auth_events',
}

export enum ExchangeTypes {
  TOPIC = 'topic',
  DIRECT = 'direct',
  FANOUT = 'fanout',
  HEADERS = 'headers',
}


export const ExchangesTypesMap: Record<Exchanges, ExchangeTypes> = {
  [Exchanges.USER_EVENTS]: ExchangeTypes.TOPIC,
  [Exchanges.ORDER_EVENTS]: ExchangeTypes.TOPIC,
  [Exchanges.AUTH_EVENTS]: ExchangeTypes.TOPIC,
};
