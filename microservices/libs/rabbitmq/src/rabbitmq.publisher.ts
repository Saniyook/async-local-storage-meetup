import { Options } from 'amqplib';
import { AsyncStorageService } from '@app/async-storage';
import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';
import { forwardRef, Inject, Injectable, Scope } from '@nestjs/common';

import { RABBITMQ_MODULE_OPTIONS } from './rabbitmq.module-definition';
import { RabbitMQModuleOptions, RabbitMQMetadata } from './rabbitmq.interfaces';

@Injectable({ scope: Scope.TRANSIENT })
export class RabbitMqPublisher {
  private exchange!: string;
  constructor(
    @Inject(forwardRef(() => AsyncStorageService))
    private asyncStorageService: AsyncStorageService,
    private amqpConnection: AmqpConnection,
    @Inject(RABBITMQ_MODULE_OPTIONS)
    private moduleOptions: RabbitMQModuleOptions
  ) {}

  async publish(routingKey: string, msg: any, options?: Options.Publish) {
    if (!this.exchange) {
      throw new Error(`No exchange registered`);
    }

    const asyncStore = this.asyncStorageService.getStore() || {};

    const metadata: RabbitMQMetadata = {
      ...asyncStore,
      ...options?.headers,
      'x-from': this.moduleOptions.microserviceName,
    };

    return this.amqpConnection.publish(this.exchange, routingKey, msg, {
      ...options,
      headers: metadata,
    });
  }

  registerExchange(exchange: string) {
    if (!this.exchange) {
      this.exchange = exchange;
    } else {
      throw new Error(`Exchange ${this.exchange} already registered`);
    }
  }
}
