import { Exchanges } from '@app/rabbitmq/const/exchanges.enum';
import { RabbitMQMethodsDecorator } from '@app/rabbitmq/rabbitmq-methods.decorator';

import { userEventsOptionsMapper } from './user.options-mapper';

export const UserEventsRabbitMQConsumerMethods = RabbitMQMethodsDecorator(
  Exchanges.USER_EVENTS,
  userEventsOptionsMapper,
  ['userCreated'],
  []
);
