import {
  QueueOptions,
  RabbitMQOptionsMapper,
} from '@app/rabbitmq/rabbitmq.interfaces';

import { UserEventsRoutingKeys } from './const';
import { IUserEventsRabbitMQConsumer } from './user-event.interface';

export const userEventsOptionsMapper: RabbitMQOptionsMapper<
  IUserEventsRabbitMQConsumer
> = (method: keyof IUserEventsRabbitMQConsumer): QueueOptions => {
  switch (method) {
    case 'userCreated':
      return {
        routingKey: UserEventsRoutingKeys.USER_CREATED,
        queue: UserEventsRoutingKeys.USER_CREATED,
      };
  }
};
