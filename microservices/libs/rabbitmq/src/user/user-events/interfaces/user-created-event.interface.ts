import { User } from '@app/shared-types/user';

export interface UserCreatedEvent {
  user: User;
}
