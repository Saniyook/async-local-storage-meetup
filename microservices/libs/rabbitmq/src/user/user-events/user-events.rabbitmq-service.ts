import { Options } from 'amqplib';
import { Injectable } from '@nestjs/common';

import { UserEventsRoutingKeys } from './const';
import { Exchanges } from '../../const/exchanges.enum';
import { RabbitMQService } from '../../rabbitmq.service';
import { UserCreatedEvent } from './interfaces/user-created-event.interface';

@Injectable()
export class UserEventsRabbitMQService extends RabbitMQService(
  Exchanges.USER_EVENTS
) {
  userCreated(message: UserCreatedEvent, options?: Options.Publish) {
    this.publisher.publish(
      UserEventsRoutingKeys.USER_CREATED,
      message,
      options
    );
  }
}
