import { ConsumeMessage } from 'amqplib';
import { UserCreatedEvent } from './interfaces/user-created-event.interface';

export interface IUserEventsRabbitMQConsumer {
  userCreated(message: UserCreatedEvent, amqpMsg: ConsumeMessage): void;
}
