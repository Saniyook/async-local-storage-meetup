import { Inject, Injectable } from '@nestjs/common';
import { RabbitMqPublisher } from './rabbitmq.publisher';

export function RabbitMQService(exchange: string) {
  const Class = class AbstractRabbitMQService {
    constructor(public publisher: RabbitMqPublisher) {
      publisher.registerExchange(exchange);
    }
  };

  Injectable()(Class);
  Inject(RabbitMqPublisher)(Class, 'constructor', 0);

  return Class;
}
