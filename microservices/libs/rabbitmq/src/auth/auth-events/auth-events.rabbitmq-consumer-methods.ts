import { Exchanges } from '@app/rabbitmq/const/exchanges.enum';
import { RabbitMQMethodsDecorator } from '@app/rabbitmq/rabbitmq-methods.decorator';

import { authEventsOptionsMapper } from './user.options-mapper';

export const AuthEventsRabbitMQConsumerMethods = RabbitMQMethodsDecorator(
  Exchanges.USER_EVENTS,
  authEventsOptionsMapper,
  ['userSignedIn', 'userSignedUp'],
  []
);
