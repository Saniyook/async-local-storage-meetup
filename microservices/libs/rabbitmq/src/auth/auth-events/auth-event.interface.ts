import { ConsumeMessage } from 'amqplib';
import { UserSignedInEvent } from './interfaces/user-signed-in-event.interface';

export interface IAuthEventsRabbitMQConsumer {
  userSignedIn(message: UserSignedInEvent, amqpMsg: ConsumeMessage): void;
  userSignedUp(message: UserSignedInEvent, amqpMsg: ConsumeMessage): void;
}
