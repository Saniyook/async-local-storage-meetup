export interface UserSignedInEvent {
  userId: string;
}
