export interface UserSignedUpEvent {
  userId: string;
}
