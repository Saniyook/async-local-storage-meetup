import { Options } from 'amqplib';
import { Injectable } from '@nestjs/common';

import { AuthEventsRoutingKeys } from './const';
import { Exchanges } from '../../const/exchanges.enum';
import { RabbitMQService } from '../../rabbitmq.service';
import { UserSignedInEvent } from './interfaces/user-signed-in-event.interface';
import { UserSignedUpEvent } from './interfaces/user-signed-up-event.interface';

@Injectable()
export class AuthEventsRabbitMQService extends RabbitMQService(
  Exchanges.USER_EVENTS
) {
  userSignedIn(message: UserSignedInEvent, options?: Options.Publish) {
    this.publisher.publish(
      AuthEventsRoutingKeys.USER_SIGNED_IN,
      message,
      options
    );
  }

  userSignedUp(message: UserSignedUpEvent, options?: Options.Publish) {
    this.publisher.publish(
      AuthEventsRoutingKeys.USER_SIGNED_UP,
      message,
      options
    );
  }
}
