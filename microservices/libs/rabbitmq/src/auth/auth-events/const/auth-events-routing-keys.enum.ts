export enum AuthEventsRoutingKeys {
  USER_SIGNED_IN = 'user.signed_in',
  USER_SIGNED_UP = 'user.signed_up',
}
