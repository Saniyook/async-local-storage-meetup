import {
  QueueOptions,
  RabbitMQOptionsMapper,
} from '@app/rabbitmq/rabbitmq.interfaces';

import { AuthEventsRoutingKeys } from './const';
import { IAuthEventsRabbitMQConsumer } from './auth-event.interface';

export const authEventsOptionsMapper: RabbitMQOptionsMapper<
  IAuthEventsRabbitMQConsumer
> = (method: keyof IAuthEventsRabbitMQConsumer): QueueOptions => {
  switch (method) {
    case 'userSignedIn':
      return {
        routingKey: AuthEventsRoutingKeys.USER_SIGNED_IN,
        queue: AuthEventsRoutingKeys.USER_SIGNED_IN,
      };

    case 'userSignedUp':
      return {
        routingKey: AuthEventsRoutingKeys.USER_SIGNED_UP,
        queue: AuthEventsRoutingKeys.USER_SIGNED_UP,
      };
  }
};
