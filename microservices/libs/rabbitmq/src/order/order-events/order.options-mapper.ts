import {
  QueueOptions,
  RabbitMQOptionsMapper,
} from '@app/rabbitmq/rabbitmq.interfaces';

import { OrderEventsRoutingKeys } from './const';
import { IOrderEventsRabbitMQConsumer } from './order-event.interface';

export const orderEventsOptionsMapper: RabbitMQOptionsMapper<
  IOrderEventsRabbitMQConsumer
> = (method: keyof IOrderEventsRabbitMQConsumer): QueueOptions => {
  switch (method) {
    case 'orderCreated':
      return {
        routingKey: OrderEventsRoutingKeys.ORDER_CREATED,
        queue: OrderEventsRoutingKeys.ORDER_CREATED,
      };
  }
};
