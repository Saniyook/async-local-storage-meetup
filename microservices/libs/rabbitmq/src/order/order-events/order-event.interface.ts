import { ConsumeMessage } from 'amqplib';
import { OrderCreatedEvent } from './interfaces/order-created-event.interface';

export interface IOrderEventsRabbitMQConsumer {
  orderCreated(message: OrderCreatedEvent, amqpMsg: ConsumeMessage): void;
}
