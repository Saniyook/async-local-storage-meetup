import { Order } from '@app/shared-types/order';

export interface OrderCreatedEvent {
  order: Order;
}
