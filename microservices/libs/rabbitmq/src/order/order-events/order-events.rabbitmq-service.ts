import { Options } from 'amqplib';
import { Injectable } from '@nestjs/common';

import { OrderEventsRoutingKeys } from './const';
import { Exchanges } from '../../const/exchanges.enum';
import { RabbitMQService } from '../../rabbitmq.service';
import { OrderCreatedEvent } from './interfaces/order-created-event.interface';

@Injectable()
export class OrderEventsRabbitMQService extends RabbitMQService(
  Exchanges.USER_EVENTS
) {
  orderCreated(message: OrderCreatedEvent, options?: Options.Publish) {
    this.publisher.publish(
      OrderEventsRoutingKeys.ORDER_CREATED,
      message,
      options
    );
  }
}
