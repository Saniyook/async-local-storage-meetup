import { Exchanges } from '@app/rabbitmq/const/exchanges.enum';
import { RabbitMQMethodsDecorator } from '@app/rabbitmq/rabbitmq-methods.decorator';

import { orderEventsOptionsMapper } from './order.options-mapper';

export const OrderEventsRabbitMQConsumer = RabbitMQMethodsDecorator(
  Exchanges.USER_EVENTS,
  orderEventsOptionsMapper,
  ['orderCreated'],
  []
);
