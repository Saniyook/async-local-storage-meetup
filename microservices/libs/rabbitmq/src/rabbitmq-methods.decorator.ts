import { RabbitRPC, RabbitSubscribe } from '@golevelup/nestjs-rabbitmq';
import { SetMetadata } from '@nestjs/common';
import { Exchanges } from './const/exchanges.enum';
import { RabbitMQOptionsMapper } from './rabbitmq.interfaces';
import { RABBITMQ_CONSUMER } from './const/inject-keys.const';

export const RabbitMQMethodsDecorator =
  <
    T,
    SubscribeMethod extends Exclude<keyof T, RpcMethod>,
    RpcMethod extends Exclude<keyof T, SubscribeMethod>
  >(
    exchange: Exchanges,
    mapper: RabbitMQOptionsMapper<T>,
    subscribeMethods: SubscribeMethod[],
    rpcMethods: RpcMethod[]
  ): ((microserviceName?: string) => ClassDecorator) =>
  (microserviceName) =>
  // eslint-disable-next-line @typescript-eslint/ban-types
  (constructor: Function) => {
    SetMetadata(RABBITMQ_CONSUMER, constructor);

    subscribeMethods.forEach((method) => {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method
      );

      const options = mapper(method);

      if (microserviceName && options?.queue) {
        options.queue = `${options.queue}.${microserviceName}`;
      }

      if (descriptor) {
        RabbitSubscribe({ ...options, exchange })(
          constructor.prototype[method],
          method as string,
          descriptor
        );
      }
    });

    rpcMethods.forEach((method) => {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method
      );

      const options = mapper(method);

      RabbitRPC({ ...options, exchange })(
        constructor.prototype[method],
        method as string,
        descriptor
      );
    });
  };
