import { Options } from 'amqplib';
import { AsyncStore } from '@app/async-storage';
import { ModuleMetadata, Type } from '@nestjs/common';

export interface QueueOptions {
  routingKey: string;
  queue?: string;
}

export interface QueueMeta extends QueueOptions {
  isRpc: boolean;
}

export interface RabbitMQModuleOptions {
  uri: string;
  microserviceName: string;
}

export type RabbitMQMetadata = Options.Publish['headers'] & {
  'x-from': RabbitMQModuleOptions['microserviceName'];
} & AsyncStore;

export interface RabbitMQModuleOptionsFactory {
  createRabbitMQModuleOptions():
    | Promise<RabbitMQModuleOptions>
    | RabbitMQModuleOptions;
}

export interface RabbitMQModuleAsyncOptions
  extends Pick<ModuleMetadata, 'imports'> {
  useFactory?: (
    ...args: any[]
  ) => Promise<RabbitMQModuleOptions> | RabbitMQModuleOptions;
  inject?: any[];
  useExisting?: Type<RabbitMQModuleOptionsFactory>;
  useClass?: Type<RabbitMQModuleOptionsFactory>;
}

export type RabbitMQOptionsMapper<T> = (key: keyof T) => QueueOptions;
