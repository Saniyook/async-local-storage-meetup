import { Type } from '@nestjs/common';
import { RABBITMQ_CONSUMER } from '../const/inject-keys.const';

export const isRabbitMqConsumer = (Class: Type<any>) =>
  Reflect.hasOwnMetadata(RABBITMQ_CONSUMER, Class);
