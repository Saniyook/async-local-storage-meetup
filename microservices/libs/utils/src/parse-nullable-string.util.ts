export const parseNullableString = (str: string) => {
  switch (str) {
    case 'null':
      return null;

    case 'undefined':
      return undefined;

    default:
      return str;
  }
};
