import set from 'lodash/set';

export const substituteDeep = (configWithEnvNames: Record<string, unknown>) => {
  const result = {};

  function substituteVars(map: Record<string, unknown>, pathTo: string[] = []) {
    for (const prop in map) {
      const value = map[prop];

      if (typeof value === 'string') {
        const envVar = process.env[value]?.replace(/\\n/g, '\n');
        if (envVar) {
          set(result, pathTo.concat(prop).join('.'), envVar);
        }
      } else {
        substituteVars(value as Record<string, unknown>, pathTo.concat(prop));
      }
    }
  }

  substituteVars(configWithEnvNames);
  return result;
};
