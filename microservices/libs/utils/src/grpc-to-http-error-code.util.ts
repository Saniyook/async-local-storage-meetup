import { HttpStatus } from '@nestjs/common';
import { Status } from '@grpc/grpc-js/build/src/constants';

export const gRPCtoHttpErrorCode = (code: Status) => {
  // gRPC to Http Mapping - https://cloud.google.com/apis/design/errors#handling_errors
  switch (code) {
    case Status.INVALID_ARGUMENT:
    case Status.FAILED_PRECONDITION:
    case Status.OUT_OF_RANGE:
      return HttpStatus.BAD_REQUEST;

    case Status.UNAUTHENTICATED:
      return HttpStatus.UNAUTHORIZED;

    case Status.PERMISSION_DENIED:
      return HttpStatus.FORBIDDEN;

    case Status.NOT_FOUND:
      return HttpStatus.NOT_FOUND;

    case Status.ABORTED:
    case Status.ALREADY_EXISTS:
      return HttpStatus.CONFLICT;

    case Status.RESOURCE_EXHAUSTED:
      return HttpStatus.TOO_MANY_REQUESTS;

    case Status.CANCELLED:
      return 499;

    case Status.DATA_LOSS:
    case Status.UNKNOWN:
    case Status.INTERNAL:
      return HttpStatus.INTERNAL_SERVER_ERROR;

    case Status.UNAVAILABLE:
      return HttpStatus.SERVICE_UNAVAILABLE;

    case Status.DEADLINE_EXCEEDED:
      return HttpStatus.GATEWAY_TIMEOUT;

    case Status.UNIMPLEMENTED:
      return HttpStatus.NOT_IMPLEMENTED;

    default:
      return HttpStatus.BAD_GATEWAY;
  }
};
