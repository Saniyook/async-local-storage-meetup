export interface Order {
  id: string;
  userId: string;
  createdAt: Date;
}
