import {
  Transport,
  GrpcOptions,
  ClientProviderOptions,
} from '@nestjs/microservices';
import { resolve } from 'path';

import {
  ANALYTIC_PACKAGE_NAME,
  AUTH_PACKAGE_NAME,
  ORDER_PACKAGE_NAME,
  USER_PACKAGE_NAME,
} from './generated';

const transformToMicroserviceOptions = (
  providerOptions: GrpcOptions
): GrpcOptions => ({
  ...providerOptions,
  options: {
    ...providerOptions.options,
    url: '0.0.0.0:5000',
  },
});

export const userProviderOptions: ClientProviderOptions = {
  name: USER_PACKAGE_NAME,
  transport: Transport.GRPC,
  options: {
    url: 'user:5000',
    package: USER_PACKAGE_NAME,
    protoPath: resolve(__dirname, './assets/proto/user.proto'),
  },
};

export const userMicroserviceOptions: GrpcOptions =
  transformToMicroserviceOptions(userProviderOptions);

export const orderProviderOptions: ClientProviderOptions = {
  name: ORDER_PACKAGE_NAME,
  transport: Transport.GRPC,
  options: {
    url: 'order:5000',
    package: ORDER_PACKAGE_NAME,
    protoPath: resolve(__dirname, './assets/proto/order.proto'),
  },
};

export const orderMicroserviceOptions: GrpcOptions =
  transformToMicroserviceOptions(orderProviderOptions);

export const authProviderOptions: ClientProviderOptions = {
  name: AUTH_PACKAGE_NAME,
  transport: Transport.GRPC,
  options: {
    url: 'auth:5000',
    package: AUTH_PACKAGE_NAME,
    protoPath: resolve(__dirname, './assets/proto/auth.proto'),
  },
};

export const authMicroserviceOptions: GrpcOptions =
  transformToMicroserviceOptions(authProviderOptions);

export const analyticProviderOptions: ClientProviderOptions = {
  name: ANALYTIC_PACKAGE_NAME,
  transport: Transport.GRPC,
  options: {
    url: 'analytic:5000',
    package: ANALYTIC_PACKAGE_NAME,
    protoPath: resolve(__dirname, './assets/proto/analytic.proto'),
  },
};

export const analyticMicroserviceOptions: GrpcOptions =
  transformToMicroserviceOptions(analyticProviderOptions);
