import { Observable } from 'rxjs';
import { Metadata } from '@grpc/grpc-js';
import { RpcException } from '@nestjs/microservices';

import { GrpcMetadata } from './grpc-metadata/grpc-metadata.interface';

export type DeepNullable<T> = {
  [K in keyof T]: DeepNullable<T[K]> | null;
};

export type GrpcServiceContext = GrpcServiceOptions & {
  metadata?: GrpcMetadata;
};

export type GrpcServiceOptions = {
  /**
   * default: `false`
   */
  asObservable?: boolean;
};

export interface ContextWithAsObservable extends GrpcServiceContext {
  asObservable: true;
}

export type PatchedGrpcServiceMethod = <T = any>(
  request: Record<string, any>,
  context?: GrpcServiceContext
) => Observable<T> | Promise<T>;

export type PatchedGrpcService<T extends Record<string, any>> = {
  [key in keyof T]: <Context extends GrpcServiceContext>(
    request: Parameters<T[key]>[0],
    context?: Context
  ) => ReturnType<T[key]> extends Observable<infer U>
    ? Context extends ContextWithAsObservable
      ? Observable<U>
      : Promise<U>
    : ReturnType<T[key]>;
};

export type GrpcServiceMethod<
  Request extends Record<string, any> = Record<string, any>,
  Return extends Observable<any> = Observable<any>
> = (request: Request, metadata: Metadata) => Return;

export type GrpcService = Record<string, GrpcServiceMethod>;

export type GrpcRequestValidateHandler<Method> = Method extends (
  ...args: any
) => any
  ? (...args: Parameters<Method>) => boolean
  : never;

export type GrpcServiceFallbackHandler<Method> = Method extends (
  ...args: any
) => any
  ? ReturnType<Method> extends Observable<infer U>
    ? (
        error: RpcException,
        ...args: Parameters<Method>
      ) => null | DeepNullable<U>
    : ReturnType<Method>
  : never;

export type GrpcRequestValidatorWithFallback<
  Method = (...args: any) => Observable<any>
> = {
  validate: GrpcRequestValidateHandler<Method>;
  fallbackHandler?: GrpcServiceFallbackHandler<Method>;
};

export type GrpcRequestValidatorWithCustomErrorMessage<
  Method = (...args: any) => any
> = {
  validate: GrpcRequestValidateHandler<Method>;
  errorMessage?: string;
};

export type GrpcRequestValidator<Method = (...args: any) => any> =
  | GrpcRequestValidatorWithFallback<Method>
  | GrpcRequestValidatorWithCustomErrorMessage<Method>;

export type GrpcServiceRequestValidation<Service extends Record<string, any>> =
  {
    [key in keyof Service]: GrpcRequestValidator<Service[key]>;
  };
