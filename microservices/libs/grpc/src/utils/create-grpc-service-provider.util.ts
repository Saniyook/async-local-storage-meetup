import { camelCase } from 'lodash';
import { ModuleRef } from '@nestjs/core';
import { ClientGrpc } from '@nestjs/microservices';
import { FactoryProvider, Type } from '@nestjs/common';
import { AsyncStorageService } from '@app/async-storage';

import {
  GrpcService,
  PatchedGrpcService,
  GrpcServiceRequestValidation,
} from '../grpc.interface';
import { createPatchedGrpcServiceMethod } from './create-grpc-service-method.util';

export const createGrpcServiceProvider = (
  ServiceClass: Type<any>,
  packageName: string,
  serviceName: string,
  requestValidators: GrpcServiceRequestValidation<
    Record<string, (...args: any) => any>
  >
): FactoryProvider => ({
  provide: ServiceClass,
  inject: [packageName, ModuleRef],
  useFactory: (client: ClientGrpc, moduleRef: ModuleRef) => {
    const service = client.getService<GrpcService>(serviceName);
    const pathcedService: PatchedGrpcService<any> = {};

    const asyncStorageService = moduleRef.get(AsyncStorageService, {
      strict: false,
    });

    Object.keys(service).forEach((methodName) => {
      pathcedService[methodName] = <any>(
        createPatchedGrpcServiceMethod(
          service[methodName],
          requestValidators[camelCase(methodName)],
          serviceName,
          methodName,
          asyncStorageService
        )
      );
    });

    return pathcedService;
  },
});
