import { Metadata } from '@grpc/grpc-js';
import { RpcException } from '@nestjs/microservices';
import { AsyncStorageService } from '@app/async-storage';
import { Status } from '@grpc/grpc-js/build/src/constants';
import { catchError, lastValueFrom, of, throwError } from 'rxjs';

import {
  GrpcServiceContext,
  GrpcServiceMethod,
  PatchedGrpcServiceMethod,
  GrpcRequestValidatorWithFallback,
  GrpcRequestValidatorWithCustomErrorMessage,
} from '../grpc.interface';
import { DEFAULT_GRPC_SERVICE_OPTIONS } from '../const/default-grpc-service-options.const';

export const createPatchedGrpcServiceMethod =
  (
    grpcServiceMethod: GrpcServiceMethod,
    {
      validate,
      errorMessage,
      fallbackHandler,
    }: GrpcRequestValidatorWithFallback &
      GrpcRequestValidatorWithCustomErrorMessage,
    serviceName: string,
    methodName: string,
    asyncStorageService?: AsyncStorageService
  ): PatchedGrpcServiceMethod =>
  (
    request: Record<string, any>,
    { metadata: requestMetadata, ...options }: GrpcServiceContext = {}
  ) => {
    let makeRequest: GrpcServiceMethod = grpcServiceMethod;
    const isRequestValid = validate(request);

    if (!isRequestValid) {
      makeRequest = () =>
        throwError(
          () =>
            new RpcException({
              message: errorMessage || 'ERROR_INVALID_REQUEST',
              code: Status.INVALID_ARGUMENT,
              context: request,
              fallbackHandler,
            })
        );
    }

    const meta = new Metadata();
    const requestOptions = Object.assign(
      {},
      DEFAULT_GRPC_SERVICE_OPTIONS,
      options
    );
    const contextStore = asyncStorageService?.getStore();

    if (contextStore) {
      Object.entries(contextStore).forEach(([key, value]) => {
        meta.set(key, value);
      });
    }

    if (requestMetadata) {
      Object.entries(requestMetadata).forEach(([key, value]) => {
        meta.set(key, value);
      });
    }

    const result = makeRequest(request, meta).pipe(
      catchError((err) => {
        err.message = `${err.message} <${serviceName}.${methodName}>`;
        if (fallbackHandler) {
          return of(fallbackHandler(err, request));
        }
        return throwError(() => new RpcException(err));
      })
    );

    return requestOptions?.asObservable ? result : lastValueFrom(result);
  };
