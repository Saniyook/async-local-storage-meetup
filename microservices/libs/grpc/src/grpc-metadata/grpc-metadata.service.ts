import { Metadata } from '@grpc/grpc-js';
import { Injectable } from '@nestjs/common';

import { GrpcMetadata } from './grpc-metadata.interface';

@Injectable()
export class GrpcMetadataService {
  metadataToObject(metadata: Metadata): Partial<GrpcMetadata> {
    return metadata?.getMap() || {};
  }

  objectToMetadata(obj?: GrpcMetadata) {
    const metadata = new Metadata();

    if (obj) {
      Object.entries(obj).forEach(([key, value]) => metadata.set(key, value));
    }

    return metadata;
  }
}
