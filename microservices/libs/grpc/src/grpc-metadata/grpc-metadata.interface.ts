import { AsyncStore } from '@app/async-storage';

export type GrpcMetadata = AsyncStore;
