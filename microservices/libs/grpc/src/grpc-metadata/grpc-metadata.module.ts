import { Module } from '@nestjs/common';

import { GrpcMetadataService } from './grpc-metadata.service';

@Module({
  providers: [GrpcMetadataService],
  exports: [GrpcMetadataService],
})
export class GrpcMetadataModule {}
