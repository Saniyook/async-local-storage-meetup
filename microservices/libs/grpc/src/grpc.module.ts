import { Global, Module } from '@nestjs/common';
import { ClientsModule } from '@nestjs/microservices';

import {
  analyticProviderOptions,
  authProviderOptions,
  orderProviderOptions,
  userProviderOptions,
} from './grpc.server-options';
import { userGrpcServiceProvider } from './grpc-clients/user';
import { orderGrpcServiceProvider } from './grpc-clients/order';
import { authGrpcServiceProvider } from './grpc-clients/auth';
import { eventsGrpcServiceProvider } from './grpc-clients/analytic';
import { GrpcMetadataModule } from './grpc-metadata/grpc-metadata.module';

const grpcServicesProviders = [
  userGrpcServiceProvider,
  orderGrpcServiceProvider,
  authGrpcServiceProvider,
  eventsGrpcServiceProvider,
];

@Global()
@Module({
  imports: [
    GrpcMetadataModule,
    ClientsModule.register([
      userProviderOptions,
      authProviderOptions,
      orderProviderOptions,
      analyticProviderOptions,
    ]),
  ],

  providers: [...grpcServicesProviders],
  exports: [...grpcServicesProviders, GrpcMetadataModule],
})
export class GrpcModule {}
