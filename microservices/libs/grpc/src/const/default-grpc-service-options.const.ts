import { GrpcServiceOptions } from '../grpc.interface';

export const DEFAULT_GRPC_SERVICE_OPTIONS: Required<GrpcServiceOptions> = {
  asObservable: false,
};
