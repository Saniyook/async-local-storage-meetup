import { ApiProperty } from '@nestjs/swagger';

import { Order } from '../../../generated';

export class OrderDto implements Order {
  @ApiProperty()
  id: string;

  @ApiProperty()
  userId: string;

  @ApiProperty()
  createdAt: string;

  @ApiProperty({ type: [String] })
  items: string[];
}
