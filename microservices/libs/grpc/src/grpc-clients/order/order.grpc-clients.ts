import {
  ORDER_PACKAGE_NAME,
  ORDER_SERVICE_NAME,
  OrderServiceClient,
} from '../../generated';
import { isUUID } from 'class-validator';

import {
  PatchedGrpcService,
  GrpcServiceRequestValidation,
} from '../../grpc.interface';
import { createGrpcServiceProvider } from '../../utils/create-grpc-service-provider.util';

export const OrderGrpcService = class OrderGrpcService {};
export type OrderGrpcService = PatchedGrpcService<OrderServiceClient>;

const requestValidators: GrpcServiceRequestValidation<OrderServiceClient> = {
  createOrder: {
    validate: ({ userId }) => isUUID(userId),
  },
};

export const orderGrpcServiceProvider = createGrpcServiceProvider(
  OrderGrpcService,
  ORDER_PACKAGE_NAME,
  ORDER_SERVICE_NAME,
  requestValidators
);
