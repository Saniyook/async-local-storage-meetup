import {
  AUTH_PACKAGE_NAME,
  AUTH_SERVICE_NAME,
  AuthServiceClient,
} from '../../generated';
import { isEmail } from 'class-validator';

import {
  PatchedGrpcService,
  GrpcServiceRequestValidation,
} from '../../grpc.interface';
import { createGrpcServiceProvider } from '../../utils/create-grpc-service-provider.util';

export const AuthGrpcService = class AuthGrpcService {};
export type AuthGrpcService = PatchedGrpcService<AuthServiceClient>;

const requestValidators: GrpcServiceRequestValidation<AuthServiceClient> = {
  signIn: {
    validate: ({ email }) => isEmail(email),
  },
  signUp: {
    validate: ({ email }) => isEmail(email),
  },
};

export const authGrpcServiceProvider = createGrpcServiceProvider(
  AuthGrpcService,
  AUTH_PACKAGE_NAME,
  AUTH_SERVICE_NAME,
  requestValidators
);
