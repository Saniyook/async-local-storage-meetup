import { ApiProperty } from '@nestjs/swagger';

import { User } from '../../../generated';

export class UserDto implements User {
  @ApiProperty()
  id: string;

  @ApiProperty()
  email: string;

  @ApiProperty()
  firstName: string;

  @ApiProperty()
  lastName: string;

  constructor(user: User) {
    Object.assign(this, user);
  }
}
