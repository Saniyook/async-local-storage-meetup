import {
  USER_PACKAGE_NAME,
  USER_SERVICE_NAME,
  UserServiceClient,
} from '../../generated';
import { isEmail, isUUID } from 'class-validator';

import {
  PatchedGrpcService,
  GrpcServiceRequestValidation,
} from '../../grpc.interface';
import { createGrpcServiceProvider } from '../../utils/create-grpc-service-provider.util';

export const UserGrpcService = class UserGrpcService {};
export type UserGrpcService = PatchedGrpcService<UserServiceClient>;

const requestValidators: GrpcServiceRequestValidation<UserServiceClient> = {
  create: {
    validate: ({ email }) => isEmail(email),
  },
  findOne: {
    validate: ({ email }) => isEmail(email),
  },
  getById: {
    validate: ({ userId }) => isUUID(userId),
  },
  update: {
    validate: ({ userId }) => isUUID(userId),
  },
};

export const userGrpcServiceProvider = createGrpcServiceProvider(
  UserGrpcService,
  USER_PACKAGE_NAME,
  USER_SERVICE_NAME,
  requestValidators
);
