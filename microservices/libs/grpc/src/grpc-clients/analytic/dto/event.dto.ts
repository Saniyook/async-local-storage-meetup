import { ApiProperty } from '@nestjs/swagger';
import { Event } from '../../../../src/generated';

export class EventDto implements Omit<Event, 'createdAt'> {
  @ApiProperty()
  id: number;

  @ApiProperty()
  userId?: string;

  @ApiProperty()
  traceId: string;

  @ApiProperty()
  createdAt: Date;

  @ApiProperty({ type: 'object' })
  payload: Record<string, any>;

  @ApiProperty()
  event: string;
}
