import {
  ANALYTIC_PACKAGE_NAME,
  EVENTS_SERVICE_NAME,
  EventsServiceClient,
} from '../../generated';

import {
  PatchedGrpcService,
  GrpcServiceRequestValidation,
} from '../../grpc.interface';
import { createGrpcServiceProvider } from '../../utils/create-grpc-service-provider.util';

export const EventsGrpcService = class EventsGrpcService {};
export type EventsGrpcService = PatchedGrpcService<EventsServiceClient>;

const requestValidators: GrpcServiceRequestValidation<EventsServiceClient> = {
  getList: {
    validate: () => true,
  },
};

export const eventsGrpcServiceProvider = createGrpcServiceProvider(
  EventsGrpcService,
  ANALYTIC_PACKAGE_NAME,
  EVENTS_SERVICE_NAME,
  requestValidators
);
