rm -rf dist

protoc \
--plugin=../../node_modules/.bin/protoc-gen-ts_proto \
--ts_proto_out=./src/generated/ \
--ts_proto_opt=exportCommonSymbols=false \
--ts_proto_opt=nestJs=true,addNestjsRestParameter=true,addGrpcMetadata=true,esModuleInterop=true \
--proto_path=$PWD/proto \
--ts_proto_opt=useOptionals=message \
--experimental_allow_proto3_optional \
$PWD/proto/*.proto &&

npm run generate-barrels
