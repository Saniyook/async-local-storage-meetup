import { UserDto } from '@app/grpc/user';
import { Type } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsString } from 'class-validator';

export class SignUpRequestDto {
  @ApiProperty()
  @IsEmail()
  email!: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  password!: string;
}

export class SignUpResponseDto {
  @ApiProperty()
  token!: string;

  @ApiProperty({ type: UserDto })
  @Type(() => UserDto)
  user!: UserDto;
}
