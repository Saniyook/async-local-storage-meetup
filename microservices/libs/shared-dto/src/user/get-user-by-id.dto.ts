import { ApiProperty } from '@nestjs/swagger';
import { IsUUID } from 'class-validator';

export class GetUserByIdDto {
  @ApiProperty()
  @IsUUID('4')
  userId!: string;
}
