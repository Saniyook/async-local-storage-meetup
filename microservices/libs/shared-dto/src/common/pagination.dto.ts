import { ApiProperty } from '@nestjs/swagger';
import { Expose, Transform } from 'class-transformer';
import { IsInt, IsOptional, Max, Min } from 'class-validator';

export class PaginationDto {
  @ApiProperty({ required: false, minimum: 0, maximum: 100 })
  @IsInt()
  @IsOptional()
  @Min(0)
  @Max(100)
  @Transform(({ value }) => Number(value))
  limit?: number;

  @ApiProperty({ required: false, minimum: 1, default: 1 })
  @Min(1)
  @IsOptional()
  @IsInt()
  @Transform(({ value }) => Number(value))
  page?: number = 1;

  constructor(data?: PaginationDto) {
    Object.assign(this, data);
  }
}

export class PaginationResultDto {
  @ApiProperty({ required: false, minimum: 1, maximum: 100 })
  total?: number;
}
