import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsUUID } from 'class-validator';
import { PaginationDto, PaginationResultDto } from '@app/shared-dto/common';

import { EventDto } from './event.dto';
import { Type } from 'class-transformer';

export class GetEventsListDto extends PaginationDto {
  @ApiProperty({ required: false })
  @IsOptional()
  @IsUUID()
  traceId?: string;

  @ApiProperty({ required: false })
  @IsOptional()
  @IsUUID()
  userId?: string;
}

export class GetEventsListResultDto extends PaginationResultDto {
  @ApiProperty({ type: [EventDto] })
  @Type(() => EventDto)
  events!: EventDto[];
}
