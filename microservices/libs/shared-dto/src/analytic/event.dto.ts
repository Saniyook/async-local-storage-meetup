import { ApiProperty } from '@nestjs/swagger';

export class EventDto {
  @ApiProperty()
  id!: number;

  @ApiProperty()
  userId?: string;

  @ApiProperty()
  traceId!: string;

  @ApiProperty()
  event!: string;

  @ApiProperty()
  payload!: Record<string, any>;

  @ApiProperty()
  createdAt!: Date;
}
