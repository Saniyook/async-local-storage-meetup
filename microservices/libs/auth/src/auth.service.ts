import { SignJWT, importPKCS8, importSPKI, jwtVerify } from 'jose';
import { Request, Response } from 'express';
import { Inject, Injectable } from '@nestjs/common';
import { ServiceRequestException } from '@app/exception-filters';

import { AuthModuleOptions } from './auth.interface';
import { AUTH_MODULE_OPTIONS } from './auth.modue-definition';
import { CRYPTO_ALGORITHM } from './const/crypto-algorithm.const';
import { ACCESS_TOKEN_NAME } from './const/access-token-name.const';
import { ITokenPayload } from './interfaces/token-payload.interface';

@Injectable()
export class AuthService {
  constructor(
    @Inject(AUTH_MODULE_OPTIONS)
    private readonly options: Required<AuthModuleOptions>
  ) {}

  getAccessTokenFromRequest(req: Request): string | undefined {
    let token: string | undefined;

    switch (this.options.from) {
      case 'cookies': {
        token =
          req.signedCookies?.[ACCESS_TOKEN_NAME] ||
          req.cookies?.[ACCESS_TOKEN_NAME];
        break;
      }

      case 'headers': {
        const headerToken = req.get(ACCESS_TOKEN_NAME);

        if (headerToken) {
          const bearer = headerToken.split(' ')[0];
          const tokenContent = headerToken.split(' ')[1];

          if (bearer === 'Bearer' && tokenContent) {
            token = tokenContent;
          }
        }
        break;
      }
    }

    return token;
  }

  setAccessTokenToResponse(response: Response, token: string) {
    response.cookie(ACCESS_TOKEN_NAME, token, {
      httpOnly: true,
      domain: this.options.domain,
      sameSite: this.options.sameSite,
      secure: this.options.secure,
      signed: true,
    });
  }

  async getTokenPayload(token: string): Promise<ITokenPayload> {
    const { publicKey } = this.options;

    if (!publicKey) {
      throw new ServiceRequestException('ERROR_NO_PUBLIC_KEY_PROVIDED');
    }

    const key = await importSPKI(publicKey, CRYPTO_ALGORITHM);

    const { payload } = await jwtVerify(token, key);

    return payload as ITokenPayload;
  }

  async signToken(payload: ITokenPayload) {
    const { privateKey } = this.options;

    if (!privateKey) {
      throw new ServiceRequestException('ERROR_NO_PRIVATE_KEY_PROVIDED');
    }

    const key = await importPKCS8(privateKey, CRYPTO_ALGORITHM);

    return new SignJWT(payload)
      .setProtectedHeader({ alg: CRYPTO_ALGORITHM })
      .setIssuedAt()
      .setExpirationTime(this.options.maxTokenAge)
      .sign(key);
  }
}
