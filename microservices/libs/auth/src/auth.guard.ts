import {
  CanActivate,
  ExecutionContext,
  Inject,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { isRabbitMqConsumer } from '@app/rabbitmq';

import { IS_PUBLIC } from './const/inject-keys.const';
import { AuthService } from './auth.service';
import { RequestWithTokenPayload } from './interfaces/request-with-token-payload.interface';
import { ITokenPayload } from './interfaces/token-payload.interface';
import { Reflector } from '@nestjs/core';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private readonly reflector: Reflector,
    private readonly authService: AuthService
  ) {}

  async canActivate(context: ExecutionContext) {
    const type = context.getType();
    const obj = context.getClass();
    const handler = context.getHandler();

    if (type !== 'http') {
      // Request from the service, not from the user
      return true;
    }
    if (isRabbitMqConsumer(obj)) {
      // Request from the RabbitMQ, not from the user
      return true;
    }

    const request = context
      .switchToHttp()
      .getRequest<RequestWithTokenPayload>();

    let payload: ITokenPayload | undefined;

    try {
      const token = this.authService.getAccessTokenFromRequest(request);

      if (token) {
        payload = await this.authService.getTokenPayload(token);
      }
    } catch (err) {
      console.log({ err });
      //TODO: handle token verify errors
    }

    if (payload) {
      request.tokenPayload = payload;
    }

    const isPublic = this.reflector.getAllAndOverride<boolean>(IS_PUBLIC, [
      handler,
      obj,
    ]);

    if (isPublic) {
      return true;
    }

    if (!payload) {
      throw new UnauthorizedException({
        message: 'ERROR_USER_IS_NOT_AUTHORIZED',
      });
    }

    return true;
  }
}
