export interface AuthModuleOptions {
  /**
   * Extract token from
   *
   * Default: `cookies`
   */
  from?: 'headers' | 'cookies';

  /**
   * Public key for token verification
   *
   * Can be defined with `NX_AUTH_PUBLIC_KEY` env var
   *
   * https://en.wikipedia.org/wiki/Public-key_cryptography
   */
  publicKey?: string | null;

  /**
   * Public key for token verification
   *
   * Can be defined with `NX_AUTH_PRIVATE_KEY` env var
   *
   * https://en.wikipedia.org/wiki/Public-key_cryptography
   */
  privateKey?: string | null;

  /**
   * Domain of server
   */
  domain?: string;

  /**
   * Allows you to declare if your cookie should be restricted to a first-party or same-site context
   *
   * @property {string} lax Cookies will be sent in a same-site context
   * @property {string} strict Cookies will only be sent in a first-party context and not be sent along with requests initiated by third party websites
   * @property {string} none Cookies will be sent in all contexts
   */
  sameSite?: boolean | 'lax' | 'strict' | 'none' | undefined;

  /**
   * A cookie with the Secure attribute is only sent to the server with an encrypted request over the HTTPS protocol
   */
  secure?: boolean;

  /**
   * JWT Token TTL
   */
  maxTokenAge?: number | string;

  /**
   * ApiKey authorization key
   */
  apiKey?: string;
}
