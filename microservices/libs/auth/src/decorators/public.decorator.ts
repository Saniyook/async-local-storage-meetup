import { SetMetadata } from '@nestjs/common';
import { IS_PUBLIC } from '../const/inject-keys.const';

export const Public = () => SetMetadata(IS_PUBLIC, true);
