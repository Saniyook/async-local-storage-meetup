import {
  Inject,
  Injectable,
  CallHandler,
  NestInterceptor,
  ExecutionContext,
} from '@nestjs/common';
import { map } from 'rxjs';
import { Response } from 'express';
import { SignInResponseDto, SignUpResponseDto } from '@app/shared-dto/auth';

import { AuthModuleOptions } from '../auth.interface';
import { AUTH_MODULE_OPTIONS } from '../auth.modue-definition';
import { ACCESS_TOKEN_NAME } from '../const/access-token-name.const';
import { AuthService } from '../auth.service';

@Injectable()
export class AuthInterceptor implements NestInterceptor {
  constructor(
    @Inject(AUTH_MODULE_OPTIONS) private readonly options: AuthModuleOptions,
    private readonly authService: AuthService
  ) {}

  intercept(context: ExecutionContext, next: CallHandler<any>) {
    const response: Response = context.switchToHttp().getResponse();

    return next.handle().pipe(
      map((result: SignInResponseDto | SignUpResponseDto) => {
        const { token } = result;
        this.authService.setAccessTokenToResponse(response, token);

        return result;
      })
    );
  }
}
