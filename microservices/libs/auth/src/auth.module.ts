import { Global, Module } from '@nestjs/common';

import { AuthService } from './auth.service';
import { AUTH_MODULE_OPTIONS, Configurable } from './auth.modue-definition';

@Global()
@Module({
  providers: [AuthService],
  exports: [AuthService, AUTH_MODULE_OPTIONS],
})
export class AuthModule extends Configurable {}
