import {
  SignInRequestDto,
  SignInResponseDto,
  SignUpRequestDto,
  SignUpResponseDto,
} from '@app/shared-dto/auth';

export interface AuthController {
  signIn(data: SignInRequestDto): Promise<SignInResponseDto>;
  signUp(data: SignUpRequestDto): Promise<SignUpResponseDto>;
}
