import { Request } from 'express';
import { ITokenPayload } from './token-payload.interface';

export interface RequestWithTokenPayload extends Request {
  tokenPayload?: ITokenPayload;
}
