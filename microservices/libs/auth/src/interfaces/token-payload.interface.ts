import { JWTPayload } from 'jose';

export interface ITokenPayload extends JWTPayload {
  id: string;
}
