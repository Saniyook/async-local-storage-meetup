import { ConfigurableModuleBuilder } from '@nestjs/common';
import { AuthModuleOptions } from './auth.interface';

export const {
  MODULE_OPTIONS_TOKEN: AUTH_MODULE_OPTIONS,
  ConfigurableModuleClass: Configurable,
} = new ConfigurableModuleBuilder<AuthModuleOptions>()
  .setClassMethodName('forRoot')
  .build();
