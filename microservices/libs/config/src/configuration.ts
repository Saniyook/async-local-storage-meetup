/* eslint-disable no-empty */
import { resolve } from 'path';
import { load } from 'js-yaml';
import merge from 'lodash/merge';
import { readFileSync } from 'fs';
import { substituteDeep } from '@app/utils';

export const loadConfig = <Config = Record<string, unknown>>(): Config => {
  const config = {} as Config;
  const configDir = process.env['NX_CONFIG_DIR'];
  const nodeEnv = process.env['NODE_ENV'];

  if (!configDir) {
    throw new Error('Config dir was not specified (NX_CONFIG_DIR)');
  }

  try {
    const defaultConfig = load(
      readFileSync(resolve(process.cwd(), `${configDir}/default.yml`), 'utf-8')
    ) as Config;

    merge(config, defaultConfig);
  } catch (err) {
    console.log({ err });
  }

  if (nodeEnv) {
    try {
      const defaultEnvConfig = load(
        readFileSync(
          resolve(process.cwd(), `${configDir}/${nodeEnv}.yml`),
          'utf-8'
        )
      ) as Config;

      merge(config, defaultEnvConfig);
    } catch {
      console.log(`Config for NODE_ENV=${nodeEnv} was not found`);
    }
  }

  try {
    const readedFromEnv = load(
      readFileSync(
        resolve(process.cwd(), `${configDir}/custom-environment-variables.yml`),
        'utf-8'
      )
    ) as Config;

    const mappedEnvVariables = substituteDeep(
      readedFromEnv as Record<string, unknown>
    );
    merge(config, mappedEnvVariables);
  } catch {}

  return config;
};
