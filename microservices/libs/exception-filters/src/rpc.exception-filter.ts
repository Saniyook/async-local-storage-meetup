import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
} from '@nestjs/common';
import { throwError } from 'rxjs';
import { RpcException } from '@nestjs/microservices';
import { gRPCtoHttpErrorCode } from '@app/utils';

@Catch(RpcException)
export class RpcExceptionFilter implements ExceptionFilter {
  catch(exception: RpcException, host: ArgumentsHost) {
    const err = exception.getError();
    const type = host.getType();

    switch (type) {
      case 'http': {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse();

        if ((err as any).statusCode === undefined) {
          (err as any).statusCode = gRPCtoHttpErrorCode((err as any)?.code);
        }

        const httpError = new HttpException(
          { message: (err as any)?.details || exception.message || err },
          (err as any)?.statusCode || 400
        );

        response.status(httpError.getStatus()).json(httpError.getResponse());
        return;
      }
      case 'rpc': {
        if (exception instanceof RpcException) {
          let rpcError: any = exception.getError();

          // If the error is a nested RpcException, get the error from the nested RpcException
          if (rpcError instanceof RpcException) {
            rpcError = rpcError.getError();
          }

          const message = rpcError?.details || rpcError?.message;
          const code = rpcError?.code;
          const metadata = rpcError?.metadata;
          if (message && code) {
            return throwError(() => ({
              message,
              code,
              metadata,
            }));
          }
        }
        return throwError(() => exception);
      }
    }
  }
}
