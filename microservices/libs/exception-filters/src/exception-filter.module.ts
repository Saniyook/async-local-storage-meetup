import { APP_FILTER } from '@nestjs/core';
import { Global, Module } from '@nestjs/common';

import { RpcExceptionFilter } from './rpc.exception-filter';
import { ServiceExceptionFilter } from './service.exception-filter';

const globalFilters = [RpcExceptionFilter, ServiceExceptionFilter];

@Global()
@Module({
  providers: globalFilters.map((filter) => ({
    provide: APP_FILTER,
    useClass: filter,
  })),
})
export class ExceptionFilterModule {}
