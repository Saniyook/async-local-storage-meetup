import { HttpStatus } from '@nestjs/common';
import { RpcException } from '@nestjs/microservices';
import { Status } from '@grpc/grpc-js/build/src/constants';

export class ServiceException extends RpcException {
  constructor(
    message: string,
    public grpcCode: Status,
    public httpCode: HttpStatus
  ) {
    super({
      message,
      code: grpcCode,
      statusCode: httpCode,
    });
  }
}

export class ServiceNotFoundException extends ServiceException {
  constructor(message: string) {
    super(message, Status.NOT_FOUND, HttpStatus.NOT_FOUND);
  }
}

export class ServiceRequestException extends ServiceException {
  constructor(message: string) {
    super(message, Status.INVALID_ARGUMENT, HttpStatus.BAD_REQUEST);
  }
}

export class ServiceAlreadyExistsException extends ServiceException {
  constructor(message: string) {
    super(message, Status.ALREADY_EXISTS, HttpStatus.CONFLICT);
  }
}

export class ServiceUnauthorizedException extends ServiceException {
  constructor(message: string) {
    super(message, Status.UNAUTHENTICATED, HttpStatus.UNAUTHORIZED);
  }
}

export class ServiceForbiddenException extends ServiceException {
  constructor(message: string) {
    super(message, Status.PERMISSION_DENIED, HttpStatus.FORBIDDEN);
  }
}

export class ServiceAbortedException extends ServiceException {
  constructor(message: string) {
    super(message, Status.ABORTED, HttpStatus.CONFLICT);
  }
}
