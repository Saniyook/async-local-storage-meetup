import {
  ArgumentsHost,
  Catch,
  RpcExceptionFilter,
  HttpException,
} from '@nestjs/common';
import { EMPTY, throwError } from 'rxjs';

import { ServiceException } from './exceptions';

@Catch(ServiceException)
export class ServiceExceptionFilter
  implements RpcExceptionFilter<ServiceException>
{
  catch(exception: ServiceException, host: ArgumentsHost) {
    const type = host.getType();

    switch (type) {
      case 'http': {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse();

        const err = new HttpException(
          { message: exception.message },
          exception.httpCode
        );

        response.status(err.getStatus()).json(err.getResponse());
        return EMPTY;
      }

      case 'rpc': {
        const err = exception.getError();
        const metadata = (err as any)?.metadata;
        return throwError(() => ({
          code: exception.grpcCode,
          message: exception.message,
          metadata,
        }));
      }

      case 'ws': {
        const ctx = host.switchToWs();
        const client = ctx.getClient();

        const err = new HttpException(
          { message: exception.message },
          exception.httpCode
        );

        client.send(
          JSON.stringify({
            event: 'error',
            error: err,
          })
        );
        return EMPTY;
      }

      default: {
        return EMPTY;
      }
    }
  }
}
