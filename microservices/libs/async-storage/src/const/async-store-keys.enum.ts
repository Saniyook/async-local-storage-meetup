export enum AsyncStoreKeys {
  USER_ID = 'userId',
  TRACE_ID = 'traceId',
}
