import {
  CallHandler,
  ContextType,
  ExecutionContext,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { Metadata } from '@grpc/grpc-js';
import { RequestWithTokenPayload } from '@app/auth';

import { AsyncStorageService } from './async-storage.service';

@Injectable()
export class AsyncStorageInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler) {
    const asyncStorage = AsyncStorageService.getStore();

    switch (context.getType<ContextType | 'rmq'>()) {
      case 'http': {
        const request: RequestWithTokenPayload = context
          .switchToHttp()
          .getRequest();

        return new Observable((subscriber) => {
          asyncStorage.run(
            AsyncStorageService.getInitialStoreFromHttpRequest(request),
            () => {
              next
                .handle()
                .pipe()
                .subscribe({
                  next: (res) => subscriber.next(res),
                  error: (err) => subscriber.error(err),
                  complete: () => subscriber.complete(),
                });
            }
          );
        });
      }

      case 'rpc': {
        const metadata: Metadata = context.switchToRpc().getContext();

        return new Observable((subscriber) => {
          asyncStorage.run(
            AsyncStorageService.getInitialStoreFromRpcMetadata(metadata),
            () => {
              next
                .handle()
                .pipe()
                .subscribe({
                  next: (res) => subscriber.next(res),
                  error: (err) => subscriber.error(err),
                  complete: () => subscriber.complete(),
                });
            }
          );
        });
      }

      case 'rmq': {
        const headers = context.getArgByIndex(1)?.properties?.headers;

        console.log({ headers });

        return new Observable((subscriber) => {
          asyncStorage.run(
            AsyncStorageService.getInitialStoreFromAmqpHeaders(headers),
            () => {
              next
                .handle()
                .pipe()
                .subscribe({
                  next: (res) => subscriber.next(res),
                  error: (err) => subscriber.error(err),
                  complete: () => subscriber.complete(),
                });
            }
          );
        });
      }

      default:
        return next.handle();
    }
  }
}
