import { merge } from 'lodash';
import { Options } from 'amqplib';
import { randomUUID } from 'crypto';
import { Metadata } from '@grpc/grpc-js';
import { AsyncLocalStorage } from 'async_hooks';
import { parseNullableString } from '@app/utils';
import { Injectable, Scope } from '@nestjs/common';
import { RequestWithTokenPayload } from '@app/auth';

import { AsyncStore } from './async-storage.interface';
import { AsyncStoreKeys } from './const/async-store-keys.enum';

@Injectable({ scope: Scope.DEFAULT })
export class AsyncStorageService {
  private static readonly store = new AsyncLocalStorage<AsyncStore>();

  run(store: AsyncStore, cb: (...args: any) => any) {
    AsyncStorageService.store.run(store, cb);
  }

  set<Key extends keyof AsyncStore>(key: Key, value: AsyncStore[Key]) {
    const store = AsyncStorageService.store.getStore();

    if (store) {
      store[key] = value;

      return true;
    }

    return false;
  }

  get<Key extends keyof AsyncStore>(key: Key): AsyncStore[Key] | undefined {
    const store = AsyncStorageService.store.getStore();

    if (store) {
      return store[key];
    }
  }

  getStore(): AsyncStore | undefined {
    return AsyncStorageService.store.getStore();
  }

  private static getInitialStore(mergeWith: Partial<AsyncStore> = {}) {
    const defaultStore: AsyncStore = {
      traceId: randomUUID(),
    };
    return merge({}, defaultStore, mergeWith);
  }

  static getInitialStoreFromHttpRequest(
    request: RequestWithTokenPayload
  ): AsyncStore {
    const userId = request.tokenPayload?.id;

    return this.getInitialStore({
      userId,
    });
  }

  static getInitialStoreFromRpcMetadata(metadata: Metadata): AsyncStore {
    const [userId] = metadata.get(AsyncStoreKeys.USER_ID);
    const [traceId] = metadata.get(AsyncStoreKeys.TRACE_ID);

    return this.getInitialStore({
      userId: parseNullableString(userId?.toString()),
      traceId: parseNullableString(traceId?.toString()),
    });
  }

  static getInitialStoreFromAmqpHeaders(headers: Options.Publish['headers']) {
    return this.getInitialStore({
      userId: parseNullableString(headers.userId?.toString()),
      traceId: parseNullableString(headers.traceId?.toString()),
    });
  }

  static getStore() {
    return this.store;
  }
}
