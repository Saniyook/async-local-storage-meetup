import { AsyncStoreKeys } from './const/async-store-keys.enum';

export interface AsyncStore {
  [AsyncStoreKeys.TRACE_ID]: string;
  [AsyncStoreKeys.USER_ID]?: string;
}
