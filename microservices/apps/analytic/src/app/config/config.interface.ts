import { DataSourceOptions } from 'typeorm';

export interface AppConfig {
  db: DataSourceOptions;
  service: ServiceConfig;
  rabbitMQ: RabbitMQConfig;
}

export interface RabbitMQConfig {
  user: string;
  pass: number;
  host: string;
  port: string;
  proto: string;
}

export interface ServiceConfig {
  name: string;
}
