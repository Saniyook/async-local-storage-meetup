import { GrpcModule } from '@app/grpc';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RabbitMQModule } from '@app/rabbitmq';
import { APP_INTERCEPTOR } from '@nestjs/core';
import {
  AsyncStorageInterceptor,
  AsyncStorageModule,
} from '@app/async-storage';
import { ExceptionFilterModule } from '@app/exception-filters';

import { ConfigModule } from './config/config.module';
import { EventsModule } from './events/events.module';
import { ConfigService } from './config/config.service';

@Module({
  imports: [
    GrpcModule,
    EventsModule,
    ConfigModule,
    AsyncStorageModule,
    ExceptionFilterModule,
    TypeOrmModule.forRootAsync({
      inject: [ConfigService],
      useFactory: ({ dbConfig }: ConfigService) => ({
        ...dbConfig,
        autoLoadEntities: true,
      }),
    }),
    RabbitMQModule.forRootAsync({
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        uri: configService.rabbitMQUrl,
        microserviceName: configService.serviceConfig.name,
      }),
    }),
  ],
  providers: [
    {
      provide: APP_INTERCEPTOR,
      useClass: AsyncStorageInterceptor,
    },
  ],
})
export class AppModule {}
