import { Module } from '@nestjs/common';
import { UserEventConsumer } from './event-consumers/user.event-consumer';
import { AuthEventConsumer } from './event-consumers/auth.event-consumer';
import { EventsService } from './events.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EventEntity } from './entities/event.entity';
import { EventsGrpcController } from './events.grpc-controller';

@Module({
  imports: [TypeOrmModule.forFeature([EventEntity])],
  providers: [UserEventConsumer, AuthEventConsumer, EventsService],
  controllers: [EventsGrpcController],
})
export class EventsModule {}
