import {
  GetEventsListDto,
  GetEventsListResultDto,
} from '@app/shared-dto/analytic';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

import { SaveEventDto } from './dto/save-event.dto';
import { EventEntity } from './entities/event.entity';

export class EventsService {
  constructor(
    @InjectRepository(EventEntity)
    private readonly eventRepo: Repository<EventEntity>
  ) {}

  async saveEvent(data: SaveEventDto) {
    return this.eventRepo.save(data);
  }

  async getList({
    limit,
    page,
    ...filters
  }: GetEventsListDto): Promise<GetEventsListResultDto> {
    page = page ?? 1;
    const take = limit ?? 10;
    const skip = (page - 1) * take;

    const [events, total] = await this.eventRepo.findAndCount({
      where: filters,
      take,
      skip,
    });

    return { events, total };
  }
}
