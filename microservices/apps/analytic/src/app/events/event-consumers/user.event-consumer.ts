import {
  IUserEventsRabbitMQConsumer,
  UserEventsRabbitMQConsumerMethods,
} from '@app/rabbitmq';
import { ConsumeMessage } from 'amqplib';
import { Injectable } from '@nestjs/common';
import { AsyncStorageService } from '@app/async-storage';
import { AsyncStoreKeys } from '@app/async-storage/const/async-store-keys.enum';
import { UserCreatedEvent } from '@app/rabbitmq/user/user-events/interfaces/user-created-event.interface';

import { EventsService } from '../events.service';

@Injectable()
@UserEventsRabbitMQConsumerMethods()
export class UserEventConsumer implements IUserEventsRabbitMQConsumer {
  constructor(
    private readonly eventsService: EventsService,
    private readonly asyncStorageService: AsyncStorageService
  ) {}

  userCreated(message: UserCreatedEvent, amqpMsg: ConsumeMessage): void {
    const userId = this.asyncStorageService.get(AsyncStoreKeys.USER_ID);
    const traceId = this.asyncStorageService.get(AsyncStoreKeys.TRACE_ID);

    this.eventsService.saveEvent({
      event: amqpMsg.fields.routingKey,
      traceId,
      userId,
      payload: message,
    });
  }
}
