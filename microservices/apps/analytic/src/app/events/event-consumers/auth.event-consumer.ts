import {
  AuthEventsRabbitMQConsumerMethods,
  IAuthEventsRabbitMQConsumer,
} from '@app/rabbitmq/auth';
import { ConsumeMessage } from 'amqplib';
import { Injectable } from '@nestjs/common';
import { UserSignedInEvent } from '@app/rabbitmq/auth/auth-events/interfaces/user-signed-in-event.interface';

import { EventsService } from '../events.service';
import { AsyncStorageService } from '@app/async-storage';
import { AsyncStoreKeys } from '@app/async-storage/const/async-store-keys.enum';

@Injectable()
@AuthEventsRabbitMQConsumerMethods()
export class AuthEventConsumer implements IAuthEventsRabbitMQConsumer {
  constructor(
    private readonly eventsService: EventsService,
    private readonly asyncStorageService: AsyncStorageService
  ) {}

  async userSignedIn(message: UserSignedInEvent, amqpMsg: ConsumeMessage) {
    const userId = this.asyncStorageService.get(AsyncStoreKeys.USER_ID);
    const traceId = this.asyncStorageService.get(AsyncStoreKeys.TRACE_ID);

    this.eventsService.saveEvent({
      event: amqpMsg.fields.routingKey,
      traceId,
      userId,
      payload: message,
    });
  }

  async userSignedUp(message: UserSignedInEvent, amqpMsg: ConsumeMessage) {
    const userId = this.asyncStorageService.get(AsyncStoreKeys.USER_ID);
    const traceId = this.asyncStorageService.get(AsyncStoreKeys.TRACE_ID);

    this.eventsService.saveEvent({
      event: amqpMsg.fields.routingKey,
      traceId,
      userId,
      payload: message,
    });
  }
}
