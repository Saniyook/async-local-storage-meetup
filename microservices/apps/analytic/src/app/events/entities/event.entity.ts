import {
  Column,
  Entity,
  CreateDateColumn,
  PrimaryGeneratedColumn,
} from 'typeorm';

import { EVENTS_TABLE_NAME } from '../const/events-table-name.const';

@Entity({ name: EVENTS_TABLE_NAME })
export class EventEntity {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({ type: 'uuid', nullable: true })
  userId?: string;

  @Column({ type: 'uuid', nullable: true })
  traceId: string;

  @Column({ type: 'varchar' })
  event: string;

  @Column({ type: 'json', default: '{}' })
  payload: Record<string, any>;

  @CreateDateColumn()
  createdAt: Date;
}
