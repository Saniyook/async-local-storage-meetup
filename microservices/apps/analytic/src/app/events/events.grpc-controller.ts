import {
  GetEventListRequest,
  EventsServiceController,
  EventsServiceControllerMethods,
} from '@app/grpc';

import { EventsService } from './events.service';
import { Injectable } from '@nestjs/common';

@Injectable()
@EventsServiceControllerMethods()
export class EventsGrpcController implements EventsServiceController {
  constructor(private readonly eventsService: EventsService) {}

  async getList(request: GetEventListRequest) {
    const { events, total } = await this.eventsService.getList(request);

    return {
      total,
      events: events.map((event) => ({
        ...event,
        createdAt: event.createdAt.toISOString(),
      })),
    };
  }
}
