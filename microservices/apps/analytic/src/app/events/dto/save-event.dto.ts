export class SaveEventDto {
  userId?: string;

  traceId: string;

  event: string;

  payload?: Record<string, any>;
}
