import { NestFactory } from '@nestjs/core';

import { AppModule } from './app/app.module';
import { analyticMicroserviceOptions } from '@app/grpc';

async function bootstrap() {
  const app = await NestFactory.createMicroservice(
    AppModule,
    analyticMicroserviceOptions
  );

  await app.init();
  await app.listen();
}

bootstrap();
