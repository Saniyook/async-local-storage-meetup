import { NestFactory } from '@nestjs/core';
import { userMicroserviceOptions } from '@app/grpc';

import { AppModule } from './app/app.module';

async function bootstrap() {
  const app = await NestFactory.createMicroservice(
    AppModule,
    userMicroserviceOptions
  );

  await app.init();
  await app.listen();
}

bootstrap();
