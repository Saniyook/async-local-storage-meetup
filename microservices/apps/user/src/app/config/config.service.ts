import { Inject, Injectable } from '@nestjs/common';

import { AppConfig } from './config.interface';
import { CONFIG } from './const/inject-keys.const';

@Injectable()
export class ConfigService {
  constructor(@Inject(CONFIG) private readonly config: AppConfig) {}

  get dbConfig() {
    return this.config.db;
  }

  get rabbitMQUrl() {
    const { host, user, pass, port } = this.config.rabbitMQ;

    return `amqp://${user}:${pass}@${host}:${port}`;
  }

  get serviceConfig() {
    return this.config.service;
  }
}
