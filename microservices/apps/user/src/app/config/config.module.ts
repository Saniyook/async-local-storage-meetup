import { Global, Module } from '@nestjs/common';
import { loadConfig } from '@app/config';

import { ConfigService } from './config.service';
import { CONFIG } from './const/inject-keys.const';

@Global()
@Module({
  providers: [
    ConfigService,
    {
      provide: CONFIG,
      useValue: loadConfig(),
    },
  ],
  exports: [ConfigService],
})
export class ConfigModule {}
