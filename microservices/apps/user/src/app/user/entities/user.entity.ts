import { User } from '@app/shared-types/user';

import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { USER_TABLE_NAME } from '../const/user-table-name.const';

@Entity({ name: USER_TABLE_NAME })
export class UserEntity implements User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ type: 'text' })
  email: string;

  @Column({ type: 'text', nullable: true })
  firstName: string;

  @Column({ type: 'text', nullable: true })
  lastName: string;
}
