import { Repository } from 'typeorm';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEventsRabbitMQService } from '@app/rabbitmq';
import { CreateUserDto, FindOneUserDto } from '@app/shared-dto/user';
import {
  ServiceAlreadyExistsException,
  ServiceNotFoundException,
} from '@app/exception-filters';

import { UserEntity } from './entities/user.entity';
import { UpdateUserDto } from '@app/shared-dto/user';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepo: Repository<UserEntity>,
    private readonly userEventsRabbitMQService: UserEventsRabbitMQService
  ) {}

  async create(data: CreateUserDto): Promise<UserEntity> {
    const exists = await this.userRepo.findOneBy({ email: data.email });

    if (exists) {
      throw new ServiceAlreadyExistsException('ERROR_USER_ALREADY_EXISTS');
    }

    const user = await this.userRepo.save(data);

    this.userEventsRabbitMQService.userCreated({ user });

    return user;
  }

  async findOne(data: FindOneUserDto): Promise<UserEntity | undefined> {
    return this.userRepo.findOneBy(data);
  }

  async findOneById(id: string) {
    return this.userRepo.findOneBy({ id });
  }

  async update({ userId, ...data }: UpdateUserDto) {
    const user = await this.userRepo.findOneBy({ id: userId });

    if (!user) {
      throw new ServiceNotFoundException('ERROR_USER_NOT_FOUND');
    }

    const toUpdate = this.userRepo.merge(user, data);

    return this.userRepo.save(toUpdate);
  }
}
