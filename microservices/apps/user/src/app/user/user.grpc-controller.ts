import {
  CreateUserRequest,
  FindOneUserRequest,
  GetUserByIdRequest,
  UpdateUserDataRequest,
  UserServiceController,
  UserServiceControllerMethods,
} from '@app/grpc';
import { Controller } from '@nestjs/common';

import { UserService } from './user.service';

@Controller()
@UserServiceControllerMethods()
export class UserGrpcController implements UserServiceController {
  constructor(private readonly userService: UserService) {}

  async create(request: CreateUserRequest) {
    return await this.userService.create(request);
  }

  async findOne(request: FindOneUserRequest) {
    const user = await this.userService.findOne(request);

    return {
      user,
    };
  }

  async getById({ userId }: GetUserByIdRequest) {
    const user = await this.getById({ userId });

    return {
      user,
    };
  }

  async update(request: UpdateUserDataRequest) {
    const user = await this.userService.update(request);

    return {
      user,
    };
  }
}
