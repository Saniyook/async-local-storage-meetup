import { GrpcModule } from '@app/grpc';
import { Module } from '@nestjs/common';
import { RabbitMQModule } from '@app/rabbitmq';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule as AuthLibModule } from '@app/auth';
import {
  AsyncStorageInterceptor,
  AsyncStorageModule,
} from '@app/async-storage';
import { ExceptionFilterModule } from '@app/exception-filters';

import { AuthModule } from './auth/auth.module';
import { ConfigModule } from './config/config.module';
import { ConfigService } from './config/config.service';
import { APP_INTERCEPTOR } from '@nestjs/core';

@Module({
  imports: [
    AuthModule,
    GrpcModule,
    ConfigModule,
    AsyncStorageModule,
    ExceptionFilterModule,
    TypeOrmModule.forRootAsync({
      inject: [ConfigService],
      useFactory: ({ dbConfig }: ConfigService) => ({
        ...dbConfig,
        autoLoadEntities: true,
      }),
    }),
    AuthLibModule.forRootAsync({
      inject: [ConfigService],
      useFactory: ({ authConfig }: ConfigService) => authConfig,
    }),
    RabbitMQModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        uri: configService.rabbitMQUrl,
        microserviceName: configService.serviceConfig.name,
      }),
    }),
  ],
  providers: [
    {
      provide: APP_INTERCEPTOR,
      useClass: AsyncStorageInterceptor,
    },
  ],
})
export class AppModule {}
