import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { AuthService } from './auth.service';
import { AuthGrpcController } from './auth.grpc-controllet';
import { UserPasswordEntity } from './entities/user-password.entity';

@Module({
  imports: [TypeOrmModule.forFeature([UserPasswordEntity])],
  providers: [AuthService],
  controllers: [AuthGrpcController],
})
export class AuthModule {}
