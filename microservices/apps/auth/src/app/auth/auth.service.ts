import {
  SignInRequestDto,
  SignInResponseDto,
  SignUpRequestDto,
  SignUpResponseDto,
} from '@app/shared-dto/auth';
import bcrypt from 'bcrypt';
import { Repository } from 'typeorm';
import { UserGrpcService } from '@app/grpc/user';
import { InjectRepository } from '@nestjs/typeorm';
import { AuthEventsRabbitMQService } from '@app/rabbitmq';
import { AuthService as AuthLibService } from '@app/auth';
import { ServiceRequestException } from '@app/exception-filters';

import { HASH_ROUNDS } from './const/hash-round.const';
import { UserPasswordEntity } from './entities/user-password.entity';

export class AuthService {
  constructor(
    @InjectRepository(UserPasswordEntity)
    private readonly userPassRepo: Repository<UserPasswordEntity>,
    private readonly authLibService: AuthLibService,
    private readonly userGrpcService: UserGrpcService,
    private readonly authEventsRabbitMQService: AuthEventsRabbitMQService
  ) {}

  async signIn(data: SignInRequestDto): Promise<SignInResponseDto> {
    const { user: candidate } = await this.userGrpcService.findOne({
      email: data.email,
    });

    if (!candidate) {
      throw new ServiceRequestException('ERROR_INVALID_EMAIL_OR_PASSWORD');
    }

    const userPass = await this.userPassRepo.findOneBy({
      userId: candidate.id,
    });

    const isPassValid = await bcrypt.compare(data.password, userPass.password);

    if (!isPassValid) {
      throw new ServiceRequestException('ERROR_INVALID_EMAIL_OR_PASSWORD');
    }

    const token = await this.authLibService.signToken({ id: candidate.id });

    this.authEventsRabbitMQService.userSignedIn({ userId: candidate.id });

    return {
      token,
      user: candidate,
    };
  }

  async signUp(data: SignUpRequestDto): Promise<SignUpResponseDto> {
    const user = await this.userGrpcService.create({
      email: data.email,
    });

    const hashedPass = await bcrypt.hash(data.password, HASH_ROUNDS);

    await this.userPassRepo.save({
      password: hashedPass,
      userId: user.id,
    });

    const token = await this.authLibService.signToken({ id: user.id });

    this.authEventsRabbitMQService.userSignedUp({ userId: user.id });

    return {
      token,
      user,
    };
  }
}
