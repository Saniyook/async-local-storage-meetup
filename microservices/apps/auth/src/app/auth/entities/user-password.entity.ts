import { Column, Entity, PrimaryColumn } from 'typeorm';
import { USER_PASSWORD_TABLE_NAME } from '../const/user-passwords-table-name.const';

@Entity({ name: USER_PASSWORD_TABLE_NAME })
export class UserPasswordEntity {
  @PrimaryColumn({ type: 'uuid' })
  userId: string;

  @Column({ type: 'varchar' })
  password: string;
}
