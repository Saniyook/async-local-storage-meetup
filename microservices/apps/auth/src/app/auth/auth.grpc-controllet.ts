import {
  SignInUserRequest,
  SignUpUserRequest,
  AuthServiceController,
  AuthServiceControllerMethods,
} from '@app/grpc';

import { AuthService } from './auth.service';
import { Controller } from '@nestjs/common';

@Controller()
@AuthServiceControllerMethods()
export class AuthGrpcController implements AuthServiceController {
  constructor(private readonly authService: AuthService) {}

  signIn(request: SignInUserRequest) {
    return this.authService.signIn(request);
  }

  signUp(request: SignUpUserRequest) {
    return this.authService.signUp(request);
  }
}
