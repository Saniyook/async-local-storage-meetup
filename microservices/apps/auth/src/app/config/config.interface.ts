import { DataSourceOptions } from 'typeorm';

export interface AppConfig {
  db: DataSourceOptions;
  service: ServiceConfig;
  rabbitMQ: RabbitMQConfig;
  auth: AuthConfig;
}

export interface RabbitMQConfig {
  user: string;
  pass: number;
  host: string;
  port: string;
  proto: string;
}

export interface ServiceConfig {
  name: string;
}

export interface AuthConfig {
  privateKey: string;
  maxTokenAge: string | number;
}
