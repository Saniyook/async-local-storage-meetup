import { Inject, Injectable } from '@nestjs/common';
import { CONFIG } from './const/inject-keys.const';
import { AppConfig } from './config.interface';

@Injectable()
export class ConfigService {
  constructor(@Inject(CONFIG) private readonly config: AppConfig) {}

  get dbConfig() {
    return this.config.db;
  }

  get rabbitMQUrl() {
    const { host, user, pass, port } = this.config.rabbitMQ;

    console.log(`amqp://${user}:${pass}@${host}:${port}`);

    return `amqp://${user}:${pass}@${host}:${port}`;
  }

  get serviceConfig() {
    return this.config.service;
  }

  get authConfig() {
    return this.config.auth;
  }
}
