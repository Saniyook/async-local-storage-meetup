import { NestFactory } from '@nestjs/core';
import { authMicroserviceOptions } from '@app/grpc';

import { AppModule } from './app/app.module';

async function bootstrap() {
  const app = await NestFactory.createMicroservice(
    AppModule,
    authMicroserviceOptions
  );

  await app.init();
  await app.listen();
}

bootstrap();
