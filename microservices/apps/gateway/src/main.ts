import { Logger } from '@nestjs/common';
import cookieParser from 'cookie-parser';
import { NestFactory } from '@nestjs/core';

import { AppModule } from './app/app.module';
import { ConfigService } from './app/config/config.service';
import { connectSwagger } from './app/app.documentation';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const {
    serverConfig: { basePath, host, port, proto },
    authConfig: { cookieSecret },
  } = app.get(ConfigService);

  app.setGlobalPrefix(basePath);
  connectSwagger(app);
  app.use(cookieParser(cookieSecret));

  await app.listen(port);

  Logger.log(
    `🚀 Application is running on: ${proto}://${host}:${port}/${basePath}`
  );
  Logger.log(`🚀 Documentation: ${proto}://${host}:${port}/${basePath}/docs`);
}

bootstrap();
