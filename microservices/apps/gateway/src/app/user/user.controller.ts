import {
  Get,
  Put,
  Body,
  Param,
  Query,
  HttpCode,
  Controller,
  NotFoundException,
} from '@nestjs/common';
import { Public } from '@app/auth';
import { AsyncStorageService } from '@app/async-storage';
import { UserGrpcService, UserDto } from '@app/grpc/user';
import { FindOneUserDto, GetUserByIdDto } from '@app/shared-dto/user';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { AsyncStoreKeys } from '@app/async-storage/const/async-store-keys.enum';

import { UpdateMeHttpBodyDto } from './dto/update-me.dto';

@ApiTags('User')
@Controller('user')
export class UserController {
  constructor(
    private readonly userGrpcService: UserGrpcService,
    private readonly asyncStorageService: AsyncStorageService
  ) {}

  @Get('find-one')
  @HttpCode(200)
  @Public()
  @ApiOkResponse({ status: 200, type: UserDto })
  @ApiOperation({ summary: 'Find user' })
  async findOne(@Query() query: FindOneUserDto): Promise<UserDto> {
    const { user } = await this.userGrpcService.findOne(query);

    if (!user) {
      throw new NotFoundException('ERROR_USER_NOT_FOUND');
    }

    return new UserDto(user);
  }

  @Get('/userId')
  @HttpCode(200)
  @Public()
  @ApiOkResponse({ status: 200, type: UserDto })
  @ApiOperation({ summary: 'Get user by id' })
  async getById(@Param() { userId }: GetUserByIdDto): Promise<UserDto> {
    const { user } = await this.userGrpcService.getById({ userId });

    if (!user) {
      throw new NotFoundException('ERROR_USER_NOT_FOUND');
    }

    return new UserDto(user);
  }

  @Put('/me')
  @HttpCode(200)
  @ApiOkResponse({ status: 200, type: UserDto })
  @ApiOperation({ summary: 'Update me' })
  async update(@Body() body: UpdateMeHttpBodyDto): Promise<UserDto> {
    const userId = this.asyncStorageService.get(AsyncStoreKeys.USER_ID);

    const { user } = await this.userGrpcService.update({ userId, ...body });

    return new UserDto(user);
  }
}
