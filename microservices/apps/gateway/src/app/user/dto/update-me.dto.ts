import { OmitType } from '@nestjs/swagger';
import { UpdateUserDto } from '@app/shared-dto/user';

export class UpdateMeHttpBodyDto extends OmitType(UpdateUserDto, ['userId']) {}
