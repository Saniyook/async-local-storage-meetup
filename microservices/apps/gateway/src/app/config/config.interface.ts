export interface AppConfig {
  server: ServerConfig;
  auth: AuthConfig;
}

export interface ServerConfig {
  port: number;
  host: string;
  proto: string;
  basePath: string;
}

export interface AuthConfig {
  publicKey: string;
  cookieSecret: string;
  maxTokenAge: number | string;
  sameSite: boolean | 'lax' | 'strict' | 'none';
  secure: boolean;
  from: 'headers' | 'cookies';
}
