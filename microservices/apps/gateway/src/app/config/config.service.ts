import { Inject, Injectable } from '@nestjs/common';
import { CONFIG } from './const/inject-keys.const';
import { AppConfig } from './config.interface';

@Injectable()
export class ConfigService {
  constructor(@Inject(CONFIG) private readonly config: AppConfig) {}

  get serverConfig() {
    return this.config.server;
  }

  get serverUrl() {
    const { host, port, proto } = this.config.server;

    return new URL(`${proto}://${host}:${port}`);
  }

  get authConfig() {
    return this.config.auth;
  }
}
