import {
  AsyncStorageModule,
  AsyncStorageInterceptor,
} from '@app/async-storage';
import { GrpcModule } from '@app/grpc';
import { Module } from '@nestjs/common';
import { APP_GUARD, APP_INTERCEPTOR } from '@nestjs/core';
import { ExceptionFilterModule } from '@app/exception-filters';
import { AuthGuard, AuthModule as AuthLibModule } from '@app/auth';

import { AuthModule } from './auth/auth.module';
import { UserModule } from './user/user.module';
import { ConfigModule } from './config/config.module';
import { EventsModule } from './events/events.module';
import { ConfigService } from './config/config.service';

@Module({
  imports: [
    AuthModule,
    UserModule,
    GrpcModule,
    EventsModule,
    ConfigModule,
    AsyncStorageModule,
    ExceptionFilterModule,
    AuthLibModule.forRootAsync({
      inject: [ConfigService],
      useFactory: ({ serverConfig, authConfig }: ConfigService) => ({
        domain: serverConfig.host,
        ...authConfig,
      }),
    }),
  ],
  providers: [
    {
      provide: APP_GUARD,
      useClass: AuthGuard,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: AsyncStorageInterceptor,
    },
  ],
})
export class AppModule {}
