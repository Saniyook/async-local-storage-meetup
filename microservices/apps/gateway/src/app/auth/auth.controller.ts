import {
  SignInRequestDto,
  SignInResponseDto,
  SignUpRequestDto,
  SignUpResponseDto,
} from '@app/shared-dto/auth';
import {
  Body,
  Controller,
  HttpCode,
  Post,
  UseInterceptors,
} from '@nestjs/common';
import {
  AuthInterceptor,
  AuthController as IAuthController,
  Public,
} from '@app/auth';
import { AuthGrpcService } from '@app/grpc/auth';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';

@Controller('auth')
@ApiTags('Auth')
@UseInterceptors(AuthInterceptor)
export class AuthController implements IAuthController {
  constructor(private readonly authGrpcService: AuthGrpcService) {}

  @Post('signIn')
  @Public()
  @HttpCode(200)
  @ApiOkResponse({ status: 200, type: SignInResponseDto })
  async signIn(@Body() data: SignInRequestDto): Promise<SignInResponseDto> {
    return this.authGrpcService.signIn(data);
  }

  @Post('signUp')
  @Public()
  @HttpCode(201)
  @ApiOkResponse({ status: 200, type: SignUpResponseDto })
  async signUp(@Body() data: SignUpRequestDto): Promise<SignUpResponseDto> {
    return this.authGrpcService.signUp(data);
  }
}
