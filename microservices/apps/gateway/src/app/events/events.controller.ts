import {
  GetEventsListDto,
  GetEventsListResultDto,
} from '@app/shared-dto/analytic';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { Controller, Get, HttpCode, Query } from '@nestjs/common';
import { EventsGrpcService } from '@app/grpc/analytic';

@ApiTags('Events')
@Controller('events')
export class EventsController {
  constructor(private readonly eventsGrpcService: EventsGrpcService) {}

  @Get('/list')
  @ApiOkResponse({ status: 200, type: GetEventsListResultDto })
  @HttpCode(200)
  async getList(@Query() query: GetEventsListDto) {
    return this.eventsGrpcService.getList(query);
  }
}
