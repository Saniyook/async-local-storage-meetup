import { INestApplication } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { ConfigService } from './config/config.service';

export const connectSwagger = (app: INestApplication) => {
  const {
    serverUrl,
    serverConfig: { basePath },
  } = app.get(ConfigService);

  const swaggerConfig = new DocumentBuilder()
    .setTitle('Gateway')
    .setDescription('Microservices documentation')
    .setVersion('1.0')
    .addServer(serverUrl.href)
    .build();
  const document = SwaggerModule.createDocument(app, swaggerConfig);
  SwaggerModule.setup(`${basePath}/docs`, app, document, {
    customSiteTitle: 'Gateway',
  });
};
